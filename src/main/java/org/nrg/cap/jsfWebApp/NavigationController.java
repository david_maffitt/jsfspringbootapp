package org.nrg.cap.jsfWebApp;

import org.omnifaces.cdi.Param;

import javax.enterprise.context.RequestScoped;
import javax.faces.annotation.ManagedProperty;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;

//@Component
//@Scope("request")
@Named
@RequestScoped
public class NavigationController implements Serializable {

    @Inject @Param
    private String message;


    public String showPage() {
        return ("fubar".equals(message))? "fubar": "snafu";
    }

    public void setAction(String message) {
        this.message = message;
    }
    public String getAction() {
        return message;
    }
}
