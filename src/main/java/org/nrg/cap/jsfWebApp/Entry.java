package org.nrg.cap.jsfWebApp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

//@Scope(value = "request")
//@Component(value = "entry")
@Named
@RequestScoped
public class Entry  {
//    @Autowired
    @Inject
    private MessageSource messageSource;

    public String getMessage() {
        return messageSource.getMesssage();
    }
}
